export default class People {
  constructor(){
    this.age = 0;
    this.skills = [{
      'agility':0,
      'knowledge': 0,
      'faith' : 0,
      'strength' : 0,
      'treachery': 0,
    }];
    this.profession = '';
    this.allies = [];
    this.ennemies = [];
    this.titles = [];
  }
}
